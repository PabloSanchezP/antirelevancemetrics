: '
	Script that generates the recommenders and performs the evaluation
	-Firstly, it generates the recommenders for both datasets based on the different parameters that we have for every recommender (k-NNs have neighbors and similarities, BPRMF have regularization and factors, etc)
	-Secondly, it performs the evaluation of the generated recommenders
	Some of the recommenders begin with a random initialization (BPRMF and HKV). For that reason we perform 5 repetitions for every of those recommenders. In the evaluation part of the script, we process these 5 repetitions and obtain
	an evaluation file that it is the average of the 5 previously generated files
'



# Java variables
javaCommand=java
JAR=target/AntiMetrics.jar
jvmMemory=-Xmx12G

# Files/paths variables
ttpathfiles=TrainTest
jvmMemory=-Xmx8G
recommendedFolder=RecommendationFolder
mkdir -p $recommendedFolder
resultsFolder=ResultsFolder
mkdir -p $resultsFolder


recPrefix=rec
nonaccresultsPrefix=naeval

# Evaluation variables
itemsRecommended=50
cutoffs=1,5,10,20,50


############
# Parameters of the recommenders
############

# This variables contains neighbors from 5 to 100
neighboursComplete="5 10 20 30 40 50 60 70 80 90 100"

allIterRankSysHKV="20"
allKFactorizerRankSys="10 50 100"
allLambdaFactorizerRankSys="0.1 1 10"
allAlphaFactorizerRankSys="0.1 1 10 100"

mymedialitePath=MyMediaLite-3.11/bin
BPRFactors=$allKFactorizerRankSys
BPRBiasReg="0 0.5 1"
BPRLearnRate=0.05
BPRNumIter=50
BPRRegU="0.0025 0.001 0.005 0.01 0.1 0.0005"
BPRRegJ="0.00025 0.0001 0.0005 0.001 0.01 0.00005"

# Loading similarities to all folds
# All folds

for dataset in Lastfm_NoRep_Parsed_Explicit Movielens1M_NoRep_Parsed
do
	for fold in 0 1 2 3 4
	do
		nomeclature="$dataset""$fold"
		trainfile=$ttpathfiles/"$nomeclature"_global.train
		testfile=$ttpathfiles/m"$nomeclature"_global.test


		# Random and Popularity (the ones that work with the complete indexes)
		for rankRecommenderNoSim in PopularityRecommender RandomRecommender
		do
			# Neighbors is set to 20 because this recommenderd does not use it
			outputRecfile=$recommendedFolder/"$recPrefix"_"$nomeclature"_ranksys_"$rankRecommenderNoSim"TrainItems.txt
			$javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr $rankRecommenderNoSim -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -cIndex true
		done # End pop-random
		wait


		# Skyline accuracy

		for threshold in 4
		do
			outputRecfile=$recommendedFolder/"$recPrefix"_"$nomeclature"_ranksys_"skylineRecommenderThreshold""$threshold".txt
			$javaCommand $jvmMemory -jar $JAR -o skylineRelevanceRecommender -trf $trainfile -tsf $testfile -nI $itemsRecommended -orf $outputRecfile -cIndex true -thr $threshold
		done
		wait

		# Skyline anti-relevance accuracy
		for antiRelthreshold in 2
		do
			outputRecfile=$recommendedFolder/"$recPrefix"_"$nomeclature"_ranksys_"skylineAntiRelevanceRecommenderAntiRelThreshold""$antiRelthreshold".txt
			$javaCommand $jvmMemory -jar $JAR -o skylineAntiRelevanceRecommender -trf $trainfile -tsf $testfile -nI $itemsRecommended -orf $outputRecfile -cIndex true -antiRelTh $antiRelthreshold
		done
		wait

		# UB and IB Knns
		for neighbours in $neighboursComplete
		do
			for ranksysSim in VectorCosineUserSimilarity SetJaccardUserSimilarity
			do
				outputRecfile=$recommendedFolder/"$recPrefix"_"$nomeclature"_"ranksys"_UB_k"$neighbours"_"$ranksysSim".txt
				$javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr UserNeighborhoodRecommender -rs $ranksysSim -nI $itemsRecommended -n $neighbours -orf $outputRecfile -cIndex false

			done # En ranksysSim
			wait

			for ranksysSim in VectorCosineItemSimilarity SetJaccardItemSimilarity
			do
				outputRecfile=$recommendedFolder/"$recPrefix"_"$nomeclature"_"ranksys"_IB_k"$neighbours"_"$ranksysSim".txt
				$javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr ItemNeighborhoodRecommender -rs $ranksysSim -nI $itemsRecommended -n $neighbours -orf $outputRecfile -cIndex false

			done # En ranksysSim
			wait

		done # End neighbors
		wait


		# BPRMF from mymedialite
		for repetition in 1 2 3 4 5
		do
			for factor in $BPRFactors
			do
				for bias_reg in $BPRBiasReg
				do
					for regU in $BPRRegU #Regularization for items and users is the same
					do
						regJ=$(echo "$regU/10" | bc -l)

						outputRecfile=$recommendedFolder/"$recPrefix"_"$nomeclature"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ""Rep$repetition".txt
						echo $outputRecfile
						if [ ! -f *"$outputRecfile"* ]; then
							outputRecfile2=$outputRecfile"Aux".txt
							echo "./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter""

							./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter"
							$javaCommand $jvmMemory -jar $JAR -o parseMyMediaLite -trf $outputRecfile2 $testfile $outputRecfile
							rm $outputRecfile2
						fi
					done # End regU
					wait
				done # End bias_reg
				wait
			done # End factor
			wait
		done # End repetition
		wait

		# HKV
		for repetition in 1 2 3 4 5
		do
			for rankRecommenderNoSim in MFRecommenderHKV
			do
				for kFactor in $allKFactorizerRankSys
				do
					for lambdaValue in $allLambdaFactorizerRankSys
					do
						for alphaValue in $allAlphaFactorizerRankSys
						do
							for iter in $allIterRankSysHKV
							do
								# Neighbors is put to 20 because this recommender does not use it
								outputRecfile=$recommendedFolder/"$recPrefix"_"$nomeclature"_ranksys_"$rankRecommenderNoSim"_kFactor"$kFactor"_aFactorizer"$alphaValue"_LFactorizer"$lambdaValue"Iter"$iter""Rep$repetition".txt
								$javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr $rankRecommenderNoSim -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -kFactorizer $kFactor -aFactorizer $alphaValue -lFactorizer $lambdaValue -nIFactorizer $iter -cIndex false
							done # Iterations ranksys
							wait
						done # Alpha value
						wait
					done # lambda value
					wait
				done # kFactor
				wait
			done # HKV
			wait
		done # repetition
		wait


	done # End fold
	wait
done # End dataset
wait

# RankSysEvaluation

for dataset in Movielens1M_NoRep_Parsed Lastfm_NoRep_Parsed_Explicit
do
	for fold in 0 1 2 3 4
	do
		nomeclature="$dataset""$fold"
		trainfile=$ttpathfiles/"$nomeclature"_global.train
		testfile=$ttpathfiles/m"$nomeclature"_global.test

		# Each file of Recomendations
		find $recommendedFolder/ -name "$recPrefix"_"$nomeclature"* | while read recFile; do
			recFileName=$(basename "$recFile" .txt) # extension removed
			if [[ $recFileName == *"_"* ]]; then # For filtering if we need it
			for evthreshold in 4
			do
				for antiRelTh in 2
				do
					outputResultfile=$resultsFolder/"$nonaccresultsPrefix"_RelEvTh"$evthreshold"_AntiRelTh_"$antiRelTh"_"$recFileName".txt
					$javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfile -tsf $testfile -rf $recFile -thr $evthreshold -antiRelTh $antiRelTh -rc $cutoffs -orf $outputResultfile
				done # End anti relevance threshold
				wait
			done # End th
			wait
		else
			echo $recFileName 'Not matching condition'
		fi

	done # End find
	wait


	# Average of results for HKV and BPRMF
	# BPRMF

	for evthreshold in 4
	do
		for antiRelTh in 2
		do
			for factor in $BPRFactors
			do
				for bias_reg in $BPRBiasReg
				do
					for regU in $BPRRegU # Regularization for items and users is the same
					do
						regJ=$(echo "$regU/10" | bc -l)

						matchingFile="$nonaccresultsPrefix"_RelEvTh"$evthreshold"_AntiRelTh_"$antiRelTh"_"$recPrefix"_"$nomeclature"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ"
						resultFile=$resultsFolder/"$nonaccresultsPrefix"_RelEvTh"$evthreshold"_AntiRelTh_"$antiRelTh"_"$recPrefix"_"$nomeclature"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ""AVERAGE.txt"
						$javaCommand $jvmMemory -jar $JAR -o averageSetOfResults -trf $resultsFolder/ $matchingFile $resultFile "AVERAGE"
					done # End regU
					wait
				done # End bias_reg
				wait
			done # End factor
			wait

			# HKV
			for rankRecommenderNoSim in MFRecommenderHKV
			do
				for kFactor in $allKFactorizerRankSys
				do
					for lambdaValue in $allLambdaFactorizerRankSys
					do
						for alphaValue in $allAlphaFactorizerRankSys
						do
							for iter in $allIterRankSysHKV
							do
								# Neighbors is set to 20 because this recommender does not use it
								matchingFile="$nonaccresultsPrefix"_RelEvTh"$evthreshold"_AntiRelTh_"$antiRelTh"_"$recPrefix"_"$nomeclature"_ranksys_"$rankRecommenderNoSim"_kFactor"$kFactor"_aFactorizer"$alphaValue"_LFactorizer"$lambdaValue"Iter"$iter"
								resultFile=$resultsFolder/"$nonaccresultsPrefix"_RelEvTh"$evthreshold"_AntiRelTh_"$antiRelTh"_"$recPrefix"_"$nomeclature"_ranksys_"$rankRecommenderNoSim"_kFactor"$kFactor"_aFactorizer"$alphaValue"_LFactorizer"$lambdaValue"Iter"$iter"AVERAGE.txt
								$javaCommand $jvmMemory -jar $JAR -o averageSetOfResults -trf $resultsFolder/ $matchingFile $resultFile "AVERAGE"
							done # Iterations ranksys
							wait
						done # Alpha value
						wait
					done # lambda value
					wait
				done # kFactor
				wait
			done # HKV
			wait
		done # End anti rel
		wait
	done # Done relTh
	wait


done # End fold
wait
done # End dataset
wait
