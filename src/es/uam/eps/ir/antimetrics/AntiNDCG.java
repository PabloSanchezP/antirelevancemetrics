/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics;

import java.util.Arrays;
import java.util.Set;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.rank.LogarithmicDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.IdealRelevanceModel;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;

/**
 * (Anti) Relevance model for nDCG, in which the gains of all relevant documents
 * need to be known for the normalization of the metric.
 * 
 * It is a copy of the NDCG metric from RankSys but changing the ordering of the
 * gains and selecting just the anti-relevance items
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 * 
 * @param <U> type of the users
 * @param <I> type of the items
 */
public class AntiNDCG<U, I> extends AbstractRecommendationMetric<U, I> {

	private final NDCGAntiRelevanceModel<U, I> relModel;
	private final int cutoff;
	private final RankingDiscountModel disc;

	/**
	 * Constructor.
	 *
	 * @param cutoff
	 *            maximum length of evaluated recommendation lists
	 * @param relModel
	 *            relevance model
	 */
	public AntiNDCG(int cutoff, NDCGAntiRelevanceModel<U, I> relModel) {
		this.relModel = relModel;
		this.cutoff = cutoff;
		this.disc = new LogarithmicDiscountModel();
	}

	/**
	 * Returns a score for the recommendation list.
	 *
	 * @param recommendation
	 *            the recommendation list
	 * @return score of the metric to the recommendation
	 */
	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		NDCGAntiRelevanceModel<U, I>.UserNDCGAntiRelevanceModel userRelModel = (NDCGAntiRelevanceModel<U, I>.UserNDCGAntiRelevanceModel) relModel
				.getModel(recommendation.getUser());

		double ndcg = 0.0;
		int rank = 0;

		for (Tuple2od<I> pair : recommendation.getItems()) {
			ndcg += userRelModel.gain(pair.v1) * disc.disc(rank);

			rank++;
			if (rank >= cutoff) {
				break;
			}
		}
		if (ndcg > 0) {
			ndcg /= idcg(userRelModel);
		}

		return ndcg;
	}

	private double idcg(NDCGAntiRelevanceModel.UserNDCGAntiRelevanceModel relModel) {
		double[] gains = relModel.getGainValues();
		Arrays.sort(gains);

		double idcg = 0;
		int n = Math.min(cutoff, gains.length);
		int m = gains.length;

		for (int rank = 0; rank < n; rank++) {
			idcg += gains[m - rank - 1] * disc.disc(rank);
		}

		return idcg;
	}

	/**
	 * Relevance model for nDCG, in which the gains of all relevant documents need
	 * to be known for the normalization of the metric.
	 *
	 * @param <U>
	 *            type of the users
	 * @param <I>
	 *            type of the items
	 */
	public static class NDCGAntiRelevanceModel<U, I> extends IdealRelevanceModel<U, I> {

		private final PreferenceData<U, I> testData;
		private final double threshold;

		/**
		 * Constructors.
		 *
		 * @param caching
		 *            are the user relevance models being cached?
		 * @param testData
		 *            test subset of preferences
		 * @param threshold
		 *            relevance threshold
		 */
		public NDCGAntiRelevanceModel(boolean caching, PreferenceData<U, I> testData, double threshold) {
			super(caching, testData.getUsersWithPreferences());
			this.testData = testData;
			this.threshold = threshold;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param user
		 *            input user
		 * @return ndcg relevance model for input user
		 */
		@Override
		protected UserNDCGAntiRelevanceModel get(U user) {
			return new UserNDCGAntiRelevanceModel(user);
		}

		/**
		 * User relevance model for nDCG.
		 *
		 */
		public class UserNDCGAntiRelevanceModel implements IdealRelevanceModel.UserIdealRelevanceModel<U, I> {

			private final Object2DoubleMap<I> gainMap;

			/**
			 * Constructor.
			 *
			 * @param user
			 *            user whose relevance model is computed
			 */
			public UserNDCGAntiRelevanceModel(U user) {
				this.gainMap = new Object2DoubleOpenHashMap<>();
				gainMap.defaultReturnValue(0.0);

				testData.getUserPreferences(user).filter(iv -> iv.v2 <= threshold)
						.forEach(iv -> gainMap.put(iv.v1, Math.pow(2, threshold - iv.v2 + 1.0) - 1.0));
			}

			/**
			 * {@inheritDoc}
			 *
			 * @return set of relevant items
			 */
			@Override
			public Set<I> getRelevantItems() {
				return gainMap.keySet();
			}

			/**
			 * {@inheritDoc}
			 *
			 * @param item
			 *            input item
			 * @return true is item is relevant, false otherwise
			 */
			@Override
			public boolean isRelevant(I item) {
				return gainMap.containsKey(item);
			}

			/**
			 * {@inheritDoc}
			 *
			 * @param item
			 *            input item
			 * @return relevance gain of the input item
			 */
			@Override
			public double gain(I item) {
				return gainMap.getDouble(item);
			}

			/**
			 * Get the vector of gains of the relevant items.
			 *
			 * @return array of positive relevance gains
			 */
			public double[] getGainValues() {
				return gainMap.values().toDoubleArray();
			}
		}
	}

}
