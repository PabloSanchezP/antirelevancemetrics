/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics.recommenders;

import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/***
 * Skyline recommender (working with the test set).
 * 
 * It only works with the test file returning the same preferences. As it it a
 * FastRanking recommender, the higher the score (if the score is higher than a
 * specific threshold), the better.
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> type of the users
 * @param <I> type of the items
 */
public class SkylineRelevanceRecommender<U, I> extends FastRankingRecommender<U, I> {
	protected final FastPreferenceData<U, I> dataTest;
	private double relTh;

	public SkylineRelevanceRecommender(FastPreferenceData<U, I> dataTest, double relTh) {
		super(dataTest, dataTest);
		this.dataTest = dataTest;
		this.relTh = relTh;
	}

	@Override
	public Int2DoubleMap getScoresMap(int uidx) {
		Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
		scoresMap.defaultReturnValue(0.0);

		this.dataTest.getUidxPreferences(uidx).forEach(i -> {
			if (i.v2 >= relTh) {
				scoresMap.put(i.v1, i.v2);
			}
		});

		return scoresMap;
	}

}
