/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics.recommenders;

import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/***
 * Skyline Anti-Relevance recommender (working with the test set)
 * 
 * It only works with the test file returning the inverse of the preferences (if
 * the rating is lower or equal to that threshold). As it it a FastRanking
 * recommender, the higher the score, the better (so computing the inverse, the
 * lower the value the higher the score).
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> type of the users
 * @param <I> type of the items
 */
public class SkylineAntiRelevanceRecommender<U, I> extends FastRankingRecommender<U, I> {
	protected final FastPreferenceData<U, I> dataTest;
	private double antiRelTh;

	public SkylineAntiRelevanceRecommender(FastPreferenceData<U, I> dataTest, double antiRelTh) {
		super(dataTest, dataTest);
		this.dataTest = dataTest;
		this.antiRelTh = antiRelTh;
	}

	@Override
	public Int2DoubleMap getScoresMap(int uidx) {
		Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
		scoresMap.defaultReturnValue(0.0);

		this.dataTest.getUidxPreferences(uidx).forEach(i -> {
			if (i.v2 <= antiRelTh) {
				// As we want to return the items ordered by score, we need to take the inverse
				// of the socre
				scoresMap.put(i.v1, 1.0 / (1.0 + i.v2));
			}
		});

		return scoresMap;
	}

}
