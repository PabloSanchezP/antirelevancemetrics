/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.core.util.tuples.Tuple2od;
import org.ranksys.formats.parsing.Parser;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.lda.LDAModelEstimator;
import org.ranksys.lda.LDARecommender;

import cc.mallet.topics.ParallelTopicModel;
import es.uam.eps.ir.antimetrics.mains.Experiment.recommendationStrategy;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.mf.Factorization;
import es.uam.eps.ir.ranksys.mf.als.HKVFactorizer;
import es.uam.eps.ir.ranksys.mf.als.PZTFactorizer;
import es.uam.eps.ir.ranksys.mf.plsa.PLSAFactorizer;
import es.uam.eps.ir.ranksys.mf.rec.MFRecommender;
import es.uam.eps.ir.ranksys.nn.item.ItemNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.CachedItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.ItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.TopKItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.sim.SetCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.SetJaccardItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorJaccardItemSimilarity;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.SetCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.SetJaccardUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorJaccardUserSimilarity;
import es.uam.eps.ir.ranksys.rec.Recommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.PopularityRecommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.RandomRecommender;

/***
 * Class for static methods in order to produce recommendations and parse the
 * data
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class RecommendationUtils {

	/***
	 * Method to transform implicit aggregated values into explicit bounded values
	 * between 1 and 5 according to this formula: Round(w * 4 / Max_u) + 1
	 * 
	 * @param trainFile
	 *            the original train file
	 * @param outputFile
	 *            the output file (parsed to explicit)
	 * @param charactersSplit
	 *            the character to be used as splitting
	 */
	public static void implicitToExplicitMaxRatings(String trainFile, String outputFile, String charactersSplit) {
		Map<String, Integer> userMaximums = new HashMap<String, Integer>();

		int columnUser = 0;
		int columnItem = 1;
		int columnRatingImplicits = 2;
		//The first line of lastFm is the header
		boolean ignoreFirstLine = true;
		Stream<String> stream = null;
		// First read, compute every user maximums aggregated values (or scores)
		try {
			if (ignoreFirstLine) {
				stream = Files.lines(Paths.get(trainFile)).skip(1);
			} else {
				stream = Files.lines(Paths.get(trainFile));
			}
			stream.forEach(line -> {
				String[] data = line.split(charactersSplit);
				int numberList = Integer.parseInt(data[columnRatingImplicits]);
				if (userMaximums.get(data[columnUser]) == null) {
					userMaximums.put(data[columnUser], numberList);
				}
				if (userMaximums.get(data[columnUser]) < numberList) {
					userMaximums.put(data[columnUser], numberList);
				}
			});
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Now, reading again and print in the output file the bounded value
		try {
			if (ignoreFirstLine) {
				stream = Files.lines(Paths.get(trainFile)).skip(1);
			} else {
				stream = Files.lines(Paths.get(trainFile));
			}
			final PrintStream writer = new PrintStream(outputFile);
			stream.forEach(line -> {
				String[] data = line.split(charactersSplit);
				long newValue = Math.round(
						(Double.parseDouble(data[columnRatingImplicits]) * 4.0) / userMaximums.get(data[columnUser]))
						+ 1;
				writer.println(data[columnUser] + "\t" + data[columnItem] + "\t" + newValue);
			});
			writer.close();
			stream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/***
	 * Method to parse MyMedialLte recommendations to be interpretable for RankSys
	 * 
	 * @param sourceFile
	 *            the source file
	 * @param userIndexTest
	 *            the set of users to make recommendations
	 * @param destFile
	 *            the new recommender file
	 */
	public static void parseMyMediaLite(String sourceFile, FastUserIndex<Long> userIndexTest, String destFile) {
		BufferedReader br;
		PrintStream writer = null;
		try {
			br = new BufferedReader(new FileReader(sourceFile));
			String line = br.readLine();
			writer = new PrintStream(destFile);

			while (line != null) {
				if (line != null) {
					String[] fullData = line.split("\t");
					Long user = Long.parseLong(fullData[0]);
					line = fullData[1].replace("[", "");
					line = line.replace("]", "");
					String[] data = line.split(",");
					if (userIndexTest.containsUser(user)) {
						for (String itemAndPref : data) {
							Long item = Long.parseLong(itemAndPref.split(":")[0]);
							Double preference = Double.parseDouble(itemAndPref.split(":")[1]);
							writer.println(user + "\t" + item + "\t" + preference);
						}
					}

					line = br.readLine();
				}
			}
			br.close();
			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Method to obtain an array of name of files matching specific patterns
	 * 
	 * @param dirPath
	 *            the directory path
	 * @param filePatterns
	 *            the file patterns
	 * @return the array of name of files matching that pattern
	 */
	private static File[] obtainFiles(String dirPath, String filePatterns, String nameToBeIgnored) {
		File dir = new File(dirPath);

		FilenameFilter fileFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				String[] patterns = filePatterns.split(",");
				for (String pattern : patterns) {
					if (!name.contains(pattern))
						return false;
				}
				if (!name.contains(nameToBeIgnored))
					return true;
				else
					return false;
			}
		};

		File[] files = dir.listFiles(fileFilter);
		return files;
	}

	/***
	 * Method to obtain the average of different files matching a pattern
	 * 
	 * @param dirPath
	 *            the directory of the files
	 * @param filePattern
	 *            the file patterns (patterns that the name of the files have to
	 *            match). If there is more than one, they are separated by commas
	 * @param resFile
	 *            the resultFile of the average of the results
	 */
	public static void averageResults(String dirPath, String filePattern, String resFile, String nameToBeIgnored) {

		try {
			File[] files = obtainFiles(dirPath, filePattern, nameToBeIgnored);
			if (files == null || files.length == 0) {
				return;
			}
			double divide = files.length;
			Map<String, Double> map = new LinkedHashMap<String, Double>();
			for (File file : files) {
				System.out.println(file.getPath());
				Stream<String> stream = Files.lines(file.toPath());
				stream.forEach(line -> {
					String[] fullData = line.split("\t");
					if (map.get(fullData[0]) == null)
						map.put(fullData[0], Double.parseDouble(fullData[1]));
					else
						map.put(fullData[0], map.get(fullData[0]) + Double.parseDouble(fullData[1]));

				});
				stream.close();
			}
			PrintStream resultFile = new PrintStream(resFile);
			for (String metric : map.keySet()) {
				resultFile.println(metric + "\t" + (map.get(metric) / divide));
			}
			resultFile.close();
			System.out.println("Writed on " + resFile);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/***
	 * Method to retrieve the train test indexes from a file
	 * 
	 * @param completeIndexes
	 *            flag indicating if we are going to concatenate the indexes of
	 *            train and test or not
	 * @param trainFile
	 *            the train file
	 * @param testFile
	 *            the test file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple4<List<U>, List<I>, List<U>, List<I>> retrieveTrainTestIndexes(boolean completeIndexes,
			String trainFile, String testFile, Parser<U> up, Parser<I> ip) {
		List<U> usersTrain = null;
		List<I> itemsTrain = null;
		List<U> usersTest = null;
		List<I> itemsTest = null;
		if (completeIndexes) {
			Tuple2<List<U>, List<I>> userItems = getCompleteUserItems(trainFile, testFile, up, ip);
			usersTrain = userItems.v1;
			itemsTrain = userItems.v2;
			usersTest = userItems.v1;
			itemsTest = userItems.v2;
		} else {
			Tuple2<List<U>, List<I>> userItemsTrain = getUserItemsFromFile(trainFile, up, ip);
			usersTrain = userItemsTrain.v1;
			itemsTrain = userItemsTrain.v2;
			Tuple2<List<U>, List<I>> userItemsTest = getUserItemsFromFile(testFile, up, ip);
			usersTest = userItemsTest.v1;
			itemsTest = userItemsTest.v2;

		}
		return new Tuple4<>(usersTrain, itemsTrain, usersTest, itemsTest);
	}

	/***
	 * Method to obtain the user and the items from the users and items files
	 * 
	 * @param fileTrain
	 *            the train file
	 * @param fileTest
	 *            the test file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	private static <U, I> Tuple2<List<U>, List<I>> getCompleteUserItems(String fileTrain, String fileTest, Parser<U> up,
			Parser<I> ip) {
		try {
			PreferenceData<U, I> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTrain, up, ip));
			PreferenceData<U, I> testData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTest, up, ip));
			PreferenceData<U, I> totalData = new ConcatPreferenceData<>(trainData, testData);
			List<U> usersList = totalData.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = totalData.getAllItems().collect(Collectors.toList());

			System.out.println("Ordering by longs");

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	/***
	 * Method to obtain a tuple of users and items from a file
	 * 
	 * @param file
	 *            the file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	private static <U, I> Tuple2<List<U>, List<I>> getUserItemsFromFile(String file, Parser<U> up, Parser<I> ip) {
		try {
			PreferenceData<U, I> data = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(file, up, ip));
			List<U> usersList = data.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = data.getAllItems().collect(Collectors.toList());

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			System.out.println("Exception catched");
			e.printStackTrace();
			return null;
		}

	}

	/***
	 * Method to obtain a RankSys recommender according to the parameters received.
	 * The recommenders do not use all the possible parameters
	 * 
	 * @param rec
	 *            the recommender identifier
	 * @param similarity
	 *            the similarity to be used (for kNN recommenders)
	 * @param data
	 *            the data to train the recommender
	 * @param kNeighbours
	 *            the number of kNeighbours
	 * @param userIndex
	 *            the user indexes
	 * @param itemIndex
	 *            the item indexes
	 * @param kFactorizer
	 *            the number of factors for the MF approaches
	 * @param alphaF
	 *            the alpha value
	 * @param lambdaF
	 *            the lambda value
	 * @param numInteractions
	 *            the number of iterations of the MF recommenders
	 * @return the rankSys recommender
	 */
	public static Recommender<Long, Long> obtRankSysRecommeder(String rec, String similarity,
			FastPreferenceData<Long, Long> data, int kNeighbours, FastUserIndex<Long> userIndex,
			FastItemIndex<Long> itemIndex, int kFactorizer, double alphaF, double lambdaF, int numInteractions) {

		switch (rec) {
		case "RandomRecommender": {
			System.out.println("RandomRecommender");
			return new RandomRecommender<>(data, data);
		}
		case "PopularityRecommender": {
			System.out.println("PopularityRecommender");
			return new PopularityRecommender<>(data);
		}

		case "UserNeighborhoodRecommender": { // User based
			es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simUNR = obtRanksysUserSimilarity(data, similarity);
			if (simUNR == null) {
				return null;
			} else {
				System.out.println("UserNeighborhoodRecommender");
				System.out.println("kNeighs: " + kNeighbours);
				System.out.println("Sim: " + similarity);
			}
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simUNR, kNeighbours);
			return new UserNeighborhoodRecommender<>(data, urneighborhood, 1);
		}
		case "ItemNeighborhoodRecommender": { // Item based
			es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity<Long> simINR = obtRanksysItemSimilarity(data, similarity);
			if (simINR == null) {
				return null;
			} else {
				System.out.println("ItemNeighborhoodRecommender");
				System.out.println("kNeighs: " + kNeighbours);
				System.out.println("Sim: " + similarity);
			}
			ItemNeighborhood<Long> neighborhood = new TopKItemNeighborhood<>(simINR, kNeighbours);
			neighborhood = new CachedItemNeighborhood<>(neighborhood);
			return new ItemNeighborhoodRecommender<>(data, neighborhood, 1);
		}
		case "MFRecommenderHKV": { // Matrix factorization. Y. Hu, Y. Koren, C. Volinsky. Collaborative filtering for implicit feedback datasets. ICDM 2008.
			int k = kFactorizer;
			double lambda = lambdaF;
			double alpha = alphaF;
			int numIter = numInteractions;
			System.out.println("MFRecommenderHKV");
			System.out.println("kFactors: " + k);
			System.out.println("lambda: " + lambda);
			System.out.println("alpha: " + alpha);
			System.out.println("numIter: " + numIter);

			DoubleUnaryOperator confidence = x -> 1 + alpha * x;
			Factorization<Long, Long> factorization = new HKVFactorizer<Long, Long>(lambda, confidence, numIter)
					.factorize(k, data);
			return new MFRecommender<>(userIndex, itemIndex, factorization);
		}
		case "MFRecommenderPZT": {  //I. Pilászy, D. Zibriczky and D. Tikk. Fast ALS-based Matrix Factorization for Explicit and Implicit Feedback Datasets. RecSys 2010
			int k = kFactorizer;
			double lambda = lambdaF;
			double alpha = alphaF;
			int numIter = numInteractions;
			System.out.println("MFRecommenderPZT");
			System.out.println("kFactors: " + k);
			System.out.println("lambda: " + lambda);
			System.out.println("alpha: " + alpha);
			System.out.println("numIter: " + numIter);

			DoubleUnaryOperator confidence = x -> 1 + alpha * x;
			Factorization<Long, Long> factorization = new PZTFactorizer<Long, Long>(lambda, confidence, numIter)
					.factorize(k, data);
			return new MFRecommender<>(userIndex, itemIndex, factorization);
		}
		case "MFRecommenderPLSA": { // T. Hofmann. Latent Semantic Models for Collaborative Filtering. ToIS, Vol 22 No. 1, January 2004.
			int k = kFactorizer;
			int numIter = numInteractions;
			System.out.println("MFRecommenderPLSA");
			System.out.println("kFactors: " + k);
			System.out.println("numIter: " + numIter);

			Factorization<Long, Long> factorization = new PLSAFactorizer<Long, Long>(numIter).factorize(k, data);
			return new MFRecommender<>(userIndex, itemIndex, factorization);
		}
		case "LDARecommender": { 
			int k = kFactorizer;
			double alpha = alphaF;
			int numIter = numInteractions;
			double beta = 0.01;
			int burninPeriod = 50;
			System.out.println("LDARecommender");
			System.out.println("kFactors: " + k);
			System.out.println("numIter: " + numIter);
			System.out.println("beta: " + beta);
			System.out.println("burninPeriod: " + burninPeriod);

			ParallelTopicModel topicModel = null;
			try {
				topicModel = LDAModelEstimator.estimate(data, k, alpha, beta, numIter, burninPeriod);
			} catch (IOException e) {
				return null;
			}
			return new LDARecommender<>(userIndex, itemIndex, topicModel);
		}

		default:
			System.out.println("Careful. Recommender is null");
			return null;
		}

	}

	/***
	 * Method to store a ranking file of a RankSys recommender
	 *
	 * @param ranksysTrainData
	 *            the train data
	 * @param ranksystestData
	 *            the test data
	 * @param rankSysrec
	 *            the recommender
	 * @param outputFile
	 *            the output file
	 * @param numberItemsRecommend
	 *            the number of items to recommend
	 */
	public static <U, I> void ranksysWriteRanking(PreferenceData<U, I> ranksysTrainData,
			PreferenceData<U, I> ranksystestData, Recommender<U, I> rankSysrec, String outputFile,
			int numberItemsRecommend, recommendationStrategy strat) {
		PrintStream out;
		try {
			out = new PrintStream(outputFile);
			Stream<U> targetUsers = ranksystestData.getUsersWithPreferences();
			System.out.println("Users in test data " + ranksystestData.numUsersWithPreferences());
			System.out.println("Users in test data appearing in train set "
					+ ranksystestData.getUsersWithPreferences().filter(u -> ranksysTrainData.containsUser(u)).count());

			if (strat.equals(recommendationStrategy.ONLYITEMS_TEST)) {
				numberItemsRecommend = Integer.MAX_VALUE;
			}

			final int numItemsRec = numberItemsRecommend;
			targetUsers.forEach(user -> {
				if (ranksystestData.getUserPreferences(user).count() != 0L && ranksysTrainData.containsUser(user)) {
					int rank = 1;
					Recommendation<U, I> rec = rankSysrec.getRecommendation(user, numItemsRec,
							selectRecommendationPredicate(strat, user, ranksysTrainData, ranksystestData));
					for (Tuple2od<I> tup : rec.getItems()) { // Items recommended
						// We should see if the items are in train
						out.println(formatRank(user, tup.v1, tup.v2, rank));
						rank++;
					}
				}
			});
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Predicate used in RankSys to make recommendations of items not rated by the
	 * user in train
	 *
	 * @param user
	 *            the user
	 * @param trainData
	 *            the ranksysDatamodel of train
	 * @return a predicate
	 */
	private static <U, I> Predicate<I> isNotInTrain(U user, PreferenceData<U, I> trainData) {
		return p -> trainData.getUserPreferences(user).noneMatch(itemPref -> itemPref.v1.equals(p));
	}

	/**
	 * Predicate used in RankSys to make recommendations of items that appears in
	 * the test set
	 *
	 * @param user
	 *            the user
	 * @param testData
	 *            the ranksysDatamodel of test
	 * @return a predicate
	 */
	private static <U, I> Predicate<I> isInTest(U user, PreferenceData<U, I> testData) {
		return p -> testData.getUserPreferences(user).anyMatch(itemPref -> itemPref.v1.equals(p));
	}

	/***
	 * Predicate that will return true if the item has been rated in the training
	 * set (by any user)
	 * 
	 * @param trainData
	 *            the trainData
	 * @return a predicate
	 */
	private static <U, I> Predicate<I> itemWithRatingInTrain(PreferenceData<U, I> trainData) {
		return p -> trainData.getItemPreferences(p).findFirst().isPresent();
	}

	/***
	 * Train items strategy of the predicate. -Only items with at least one rating
	 * in train should be recommended. -Only items that the user have not rated in
	 * train (new items for the user).
	 * 
	 * @param user
	 *            the user
	 * @param trainData
	 *            the trainData
	 * @return a predicate
	 */
	public static <U, I> Predicate<I> trainItems(U user, PreferenceData<U, I> trainData) {
		return isNotInTrain(user, trainData).and(itemWithRatingInTrain(trainData));
	}

	/***
	 * No condition predicate (every item is considered as a predicate)
	 * 
	 * @return a predicate
	 */
	private static <I> Predicate<I> noCondition() {
		return p -> true;
	}

	/***
	 * Method to print the recommendations for all the recommenders implemented in
	 * the framework
	 * 
	 * @param user
	 *            the user identifier
	 * @param item
	 *            the item identifier
	 * @param valueItem
	 *            the score of the recommendation
	 * @param rank
	 *            the rank
	 * @return the rank
	 */
	private static String formatRank(Object user, Object item, double valueItem, int rank) {
		return user.toString() + "\t" + item.toString() + "\t" + valueItem + "\t" + rank;
	}

	/***
	 * Selection of the recommendation predicate
	 * 
	 * @param strat
	 *            the strategy of the recommendation
	 * @param user
	 *            the user
	 * @param trainData
	 *            the train data
	 * @param testData
	 *            the test data
	 * @return a predicate for recommendation
	 */
	private static <U, I> Predicate<I> selectRecommendationPredicate(recommendationStrategy strat, U user,
			PreferenceData<U, I> trainData, PreferenceData<U, I> testData) {
		switch (strat) {
		case ALL_ITEMS:
			return isNotInTrain(user, trainData);
		case NO_CONDITION:
			return noCondition();
		case ONLYITEMS_TEST:
			return isInTest(user, testData);
		case TRAIN_ITEMS:
		default:
			return trainItems(user, trainData);
		}

	}

	/**
	 * Method to obtain the recommendation strategy
	 * 
	 * @param stringRS
	 *            the recomendation strategy read
	 * @return the enumeration value or null
	 */
	public static recommendationStrategy obtRecommendationStrategy(String stringRS) {
		for (recommendationStrategy ut : recommendationStrategy.values()) {
			if (ut.toString().equals(stringRS)) {
				return ut;
			}
		}
		return null;
	}

	/**
	 * Obtain RankSys User Similarity
	 *
	 * @param data
	 *            the preference data used to compute the similarities
	 * @param similarity
	 *            the similarity
	 * @return a RankSys user similarity model
	 */
	public static <U, I> es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<U> obtRanksysUserSimilarity(
			FastPreferenceData<U, I> data, String similarity) {
		switch (similarity) {
		case "VectorCosineUserSimilarity":
			// 0.5 to make it symmetrical.
			return new VectorCosineUserSimilarity<>(data, 0.5, true);
		case "VectorJaccardUserSimilarity":
			return new VectorJaccardUserSimilarity<>(data, true);
		case "SetJaccardUserSimilarity":
			return new SetJaccardUserSimilarity<>(data, true);
		case "SetCosineUserSimilarity":
			return new SetCosineUserSimilarity<>(data, 0.5, true);
		case "PearsonUserCorrelation":

		default:
			System.out.println("RankSys user similarity is null");
			return null;
		}
	}

	/**
	 * Obtain RankSys Item Similarity
	 *
	 * @param data
	 *            the preference data used to compute the similarities
	 * @param similarity
	 *            the string identifier of the similarity
	 * @return a RankSys item similarity model
	 */
	private static es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity<Long> obtRanksysItemSimilarity(
			FastPreferenceData<Long, Long> data, String similarity) {
		switch (similarity) {
		case "VectorCosineItemSimilarity":
			// 0.5 to make it symmetrical.
			return new VectorCosineItemSimilarity<>(data, 0.5, true);
		case "VectorJaccardItemSimilarity":
			return new VectorJaccardItemSimilarity<>(data, true);
		case "SetJaccardItemSimilarity":
			return new SetJaccardItemSimilarity<>(data, true);
		case "SetCosineItemSimilarity":
			return new SetCosineItemSimilarity<>(data, 0.5, true);

		default:
			return null;
		}
	}

}
