/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics.utils;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.AbstractSystemMetric;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;
import es.uam.eps.ir.ranksys.metrics.rel.IdealRelevanceModel;

/***
 * Average recommendation metric: it does not take into account NaN users
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> type of the users
 * @param <I> type of the items
 */
public class AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<U, I> extends AbstractSystemMetric<U, I> {

	private final RecommendationMetric<U, I> metric;
	private double sum;
	private int numUsers;

	private final IdealRelevanceModel<U, I> relModel;

	/**
	 * Constructor in which the average is calculated for all the recommendations
	 * added during the calculation.
	 *
	 * @param metric
	 *            recommendation metric to be averaged
	 * @param ignoreNaN
	 *            ignore NaNs from the calculation of the average?
	 */
	public AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs(RecommendationMetric<U, I> metric,
			IdealRelevanceModel<U, I> relModel) {
		this.metric = metric;
		this.sum = 0;
		this.numUsers = 0;
		this.relModel = relModel;
	}

	/**
	 * Adds the recommendation metric to the average and returns the user value.
	 *
	 * @param recommendation
	 *            recommendation to be added
	 * @return results of the recommender metric
	 */
	public double addAndEvaluate(Recommendation<U, I> recommendation) {
		double v = metric.evaluate(recommendation);

		// If it is empty, it is not relevant
		boolean isRelevant = !(this.relModel.getModel(recommendation.getUser()).getRelevantItems().isEmpty());

		if (isRelevant) {
			if (!Double.isNaN(v)) {
				sum += v;
			} else {
				System.out.println("User: " + recommendation.getUser() + " is relevant but metric " + metric.toString()
						+ " evaluation is NaN");
			}
			numUsers++;
		}

		return v;
	}

	@Override
	public void add(Recommendation<U, I> recommendation) {
		addAndEvaluate(recommendation);
	}

	@Override
	public void combine(SystemMetric<U, I> other) {
		sum += ((AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<U, I>) other).sum;

		numUsers += ((AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<U, I>) other).numUsers;

	}

	@Override
	public double evaluate() {
		return sum / numUsers;
	}

	@Override
	public void reset() {
		this.sum = 0;
		this.numUsers = 0;
	}

}
