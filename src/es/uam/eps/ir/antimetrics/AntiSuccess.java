/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics;

import java.util.Optional;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.antimetrics.antirel.BinaryAntiRelevanceModel;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel.UserRelevanceModel;

/***
 * Metric that retrieves 1 if the recommender retrieves any anti-Relevant item
 * in the recommendation list
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> type of the users
 * @param <I> type of the items
 */
public class AntiSuccess<U, I> extends AbstractRecommendationMetric<U, I> {

	private final int cutOff;
	private final BinaryAntiRelevanceModel<U, I> antiRelModel;

	public AntiSuccess(int cutoff, BinaryAntiRelevanceModel<U, I> relModel) {
		this.cutOff = cutoff;
		this.antiRelModel = relModel;
	}

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		UserRelevanceModel<U, I> userRelModel = antiRelModel.getModel(recommendation.getUser());

		Optional<I> opItem = recommendation.getItems().stream().limit(cutOff).map(Tuple2od::v1)
				.filter(userRelModel::isRelevant).findFirst();
		if (opItem.isPresent()) {
			return 1;
		} else {
			return 0;
		}
	}

}
