/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics.mains;

import static org.ranksys.formats.parsing.Parsers.lp;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.parsing.Parser;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.formats.rec.RecommendationFormat;
import org.ranksys.formats.rec.SimpleRecommendationFormat;

import es.uam.eps.ir.antimetrics.AntiNDCG;
import es.uam.eps.ir.antimetrics.AntiRecommendationMetricComplement;
import es.uam.eps.ir.antimetrics.AntiSuccess;
import es.uam.eps.ir.antimetrics.Success;
import es.uam.eps.ir.antimetrics.antirel.BinaryAntiRelevanceModel;
import es.uam.eps.ir.antimetrics.ratio.AntiRelevanceRatio;
import es.uam.eps.ir.antimetrics.ratio.BorderlineRatio;
import es.uam.eps.ir.antimetrics.ratio.UnknownItemsRatio;
import es.uam.eps.ir.antimetrics.recommenders.SkylineAntiRelevanceRecommender;
import es.uam.eps.ir.antimetrics.recommenders.SkylineRelevanceRecommender;
import es.uam.eps.ir.antimetrics.utils.AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs;
import es.uam.eps.ir.antimetrics.utils.RecommendationUtils;
import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;

import es.uam.eps.ir.ranksys.diversity.sales.metrics.AggregateDiversityMetric;
import es.uam.eps.ir.ranksys.diversity.sales.metrics.GiniIndex;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;
import es.uam.eps.ir.ranksys.metrics.basic.AveragePrecision;
import es.uam.eps.ir.ranksys.metrics.basic.AverageRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.basic.NDCG;
import es.uam.eps.ir.ranksys.metrics.basic.Precision;
import es.uam.eps.ir.ranksys.metrics.basic.ReciprocalRank;
import es.uam.eps.ir.ranksys.metrics.rank.ExponentialDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.LogarithmicDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.NoDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.ReciprocalDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BackgroundBinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.NoRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;

import es.uam.eps.ir.ranksys.novelty.longtail.FDItemNovelty;
import es.uam.eps.ir.ranksys.novelty.longtail.PCItemNovelty;
import es.uam.eps.ir.ranksys.novelty.longtail.metrics.EFD;
import es.uam.eps.ir.ranksys.novelty.longtail.metrics.EPC;
import es.uam.eps.ir.ranksys.rec.Recommender;
import net.recommenders.rival.core.DataModel;
import net.recommenders.rival.core.SimpleParser;
import net.recommenders.rival.split.splitter.SplitterRunner;

/***
 * Main class to perform the Experiments. The idea is to generate an executable
 * JAR from this Maven project using this main class.
 * 
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class Experiment {

	// Recommendation strategy. In this case is TrainItems
	public enum recommendationStrategy {
		NO_CONDITION, TRAIN_ITEMS, ALL_ITEMS, ONLYITEMS_TEST
	};

	// All options of the arguments

	// Option of the case
	private static final String OPT_CASE = "option";

	// Train and test files
	private static final String OPT_TRAINFILE = "trainFile";
	private static final String OPT_TESTFILE = "testFile";

	// Number of items to recommend in the recommenders
	private static final String OPT_ITEMSRECOMMENDED = "itemsRecommended";

	// Neighbors to use in the knn algorithms
	private static final String OPT_NEIGH = "neighbours";

	// Result file (file use to store the recommendations or the results)
	private static final String OPT_OUTRESULTFILE = "outResultfile";

	// RankSys similarities and recommender
	private static final String OPT_RANKSYS_SIM = "ranksysSimilarity";

	// Overwrite output file or not. Default value is false.
	private static final String OPT_OVERWRITE = "outResultFileOverwrite";

	// Variables only used in rankSysEvaluation
	private static final String OPT_RECOMMENDEDFILE = "recommendedFile";

	// Cutoff of the recommendations
	private static final String OPT_CUTOFF = "ranksysCutoff";

	// Relevance threshold of the recommendations
	private static final String OPT_RELEVANCE_THRESHOLD = "relevance threshold";

	// Parameters for RankSys Recommender MatrixFactorization
	private static final String OPT_KFACTORIZER = "k factorizer value";
	private static final String OPT_ALPHAFACTORIZER = "alpha factorizer value";
	private static final String OPT_LAMBDAFACTORIZER = "lambda factorizer value";
	private static final String OPT_NUMINTERACTIONSFACTORIZER = "num interactions factorizer value";

	// Parameters for RankSys evaluation
	private static final String OPT_RANKSYSRELEVANCEMODEL = "ranksys relevance model";
	private static final String OPT_RANKSYSDISCOUNTMODEL = "ranksys discount model";
	private static final String OPT_RANKSYSBACKGROUND = "ranksys background for relevance model";
	private static final String OPT_RANKSYSBASE = "ranksys base for discount model";

	private static final String OPT_ANTIRELEVANCETHRESHOLD = "Anti relevance threshold";
	private static final String OPT_COMPUTERECANDTEST = "compute average with train and test";
	private static final String OPT_RANKSYS_REC = "ranksysRecommender";

	private static final String OPT_COMPLETEINDEXES = "completeIndexes";
	private static final String OPT_RECOMMENDATIONSTRATEGY = "recommendation strategy";

	// Some default values for recommenders as strings
	private static final String DEFAULT_COMPLETE_INDEXES = "false";

	private static final String DEFAULT_ITEMS_RECOMMENDED = "100";
	private static final String DEFAULT_RECOMMENDATION_STRATEGY = "TRAIN_ITEMS";
	private static final String DEFAULT_OVERWRITE = "false";

	public static void main(String[] args) throws Exception {
		String step = "";
		CommandLine cl = getCommandLine(args);
		if (cl == null) {
			System.out.println("Error in arguments");
			return;
		}

		// Obtain the arguments these 2 are obligatory
		step = cl.getOptionValue(OPT_CASE);
		String trainFile = cl.getOptionValue(OPT_TRAINFILE);

		System.out.println(step);
		switch (step) {

		// split files using RiVal
		case "splitsRival": {
			System.out.println(
					"-o SplitsRival -trf data_file split_type nfolds|percentage peruser? output_folder overwrite? "
							+ "training_prefix training_suffix testing_prefix testing_suffix");
			System.out.println(Arrays.toString(args));
			// First 4 arguments are the option of the case, the train file.and
			// then the others

			Properties splitProps = new Properties();
			splitProps.setProperty(SplitterRunner.DATASET_SPLITTER, args[4]); // Temporal or CrossValidation
			splitProps.setProperty(SplitterRunner.SPLIT_CV_NFOLDS, args[5]);
			splitProps.setProperty(SplitterRunner.SPLIT_RANDOM_PERCENTAGE, args[5]);
			splitProps.setProperty(SplitterRunner.SPLIT_PERUSER, args[6]);
			splitProps.setProperty(SplitterRunner.SPLIT_OUTPUT_FOLDER, args[7]);
			splitProps.setProperty(SplitterRunner.SPLIT_OUTPUT_OVERWRITE, args[8]);
			splitProps.setProperty(SplitterRunner.SPLIT_TRAINING_PREFIX, args[9]);
			splitProps.setProperty(SplitterRunner.SPLIT_TRAINING_SUFFIX, args[10]);
			splitProps.setProperty(SplitterRunner.SPLIT_TEST_PREFIX, args[11]);
			splitProps.setProperty(SplitterRunner.SPLIT_TEST_SUFFIX, args[12]);
			splitProps.setProperty(SplitterRunner.SPLIT_SEED, "20160722");

			DataModel<Long, Long> model = new SimpleParser().parseData(new File(trainFile));

			SplitterRunner.run(splitProps, model, false);
		}
			break;

		case "implicitToExplicit": {
			System.out.println("-o ImplicitToExplicit -trf originalImplicit explicitTransform charSplit");
			System.out.println(Arrays.toString(args));
			RecommendationUtils.implicitToExplicitMaxRatings(trainFile, args[4], args[5]);
		}
			break;

		case "ranksysOnlyComplete": {
			String outputFile = cl.getOptionValue(OPT_OUTRESULTFILE);
			String ranksysSimilarity = cl.getOptionValue(OPT_RANKSYS_SIM);
			String ranksysRecommender = cl.getOptionValue(OPT_RANKSYS_REC);

			String testFile = cl.getOptionValue(OPT_TESTFILE);

			// Check if we include factorizers
			String kFactorizer = cl.getOptionValue(OPT_KFACTORIZER);
			String alphaFactorizer = cl.getOptionValue(OPT_ALPHAFACTORIZER);
			String lambdaFactorizer = cl.getOptionValue(OPT_LAMBDAFACTORIZER);
			String numInteractions = cl.getOptionValue(OPT_NUMINTERACTIONSFACTORIZER);

			// Recommendation strategy
			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATIONSTRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);

			int kFactorizeri = 50;
			double alphaFactorizeri = 1;
			double lambdaFactorizeri = 0.1;
			int numInteractioni = 20;

			if (ranksysSimilarity == null) {
				System.out.println("Not working with any recommender using similarities");
			}

			if (kFactorizer != null) {
				kFactorizeri = Integer.parseInt(kFactorizer);
				System.out.println("Working with kFactorizeri=" + kFactorizeri);
			}

			if (alphaFactorizer != null) {
				alphaFactorizeri = Double.parseDouble(alphaFactorizer);
				System.out.println("Working with alphaFactorizer=" + alphaFactorizeri);
			}
			if (lambdaFactorizer != null) {
				lambdaFactorizeri = Double.parseDouble(lambdaFactorizer);
				System.out.println("Working with lambdaFactorizer=" + lambdaFactorizer);
			}
			if (numInteractions != null) {
				numInteractioni = Integer.parseInt(numInteractions);
				System.out.println("Working with numInteractions=" + numInteractions);
			}

			final int numberItemsRecommend = Integer
					.parseInt(cl.getOptionValue(OPT_ITEMSRECOMMENDED, DEFAULT_ITEMS_RECOMMENDED));
			int neighbours = Integer.parseInt(cl.getOptionValue(OPT_NEIGH));

			System.out.println(Arrays.toString(args));

			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
			} else {
				System.out.println(f + " does not exist. Computing.");
			}

			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, DEFAULT_COMPLETE_INDEXES));
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = null;

			indexes = RecommendationUtils.retrieveTrainTestIndexes(completeOrNot, trainFile, testFile, lp, lp);

			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;
			List<Long> usersTest = indexes.v3;
			List<Long> itemsTest = indexes.v4;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
			FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

			FastPreferenceData<Long, Long> trainPrefData = null;
			FastPreferenceData<Long, Long> testPrefData = null;

			trainPrefData = SimpleFastPreferenceData.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp),
					userIndexTrain, itemIndexTrain);

			Recommender<Long, Long> rankSysrec = RecommendationUtils.obtRankSysRecommeder(ranksysRecommender,
					ranksysSimilarity, trainPrefData, neighbours, userIndexTrain, itemIndexTrain, kFactorizeri,
					alphaFactorizeri, lambdaFactorizeri, numInteractioni);

			System.out.println("Analyzing " + testFile);
			System.out.println("Recommender file " + outputFile);

			testPrefData = SimpleFastPreferenceData.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp),
					userIndexTest, itemIndexTest);

			// Normal recommendation. Only recommend for test users items that have not been
			// seen in train
			RecommendationUtils.ranksysWriteRanking(trainPrefData, testPrefData, rankSysrec, outputFile,
					numberItemsRecommend, RecommendationUtils.obtRecommendationStrategy(recommendationStrategy));

		}
			break;

		case "averageSetOfResults": {
			System.out.println("-o AverageSetOfResults -trf destinationFolder matchings resultFile");
			System.out.println(Arrays.toString(args));
			RecommendationUtils.averageResults(trainFile, args[4], args[5], args[6]);
		}
			break;

		case "parseMyMediaLite": {
			System.out.println("-o ParseMyMediaLite -trf myMediaLiteRecommendation testFile newRecommendation");
			System.out.println(Arrays.toString(args));
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = RecommendationUtils
					.retrieveTrainTestIndexes(false, args[4], args[4], lp, lp);

			List<Long> usersTest = indexes.v1;
			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());

			RecommendationUtils.parseMyMediaLite(trainFile, userIndexTest, args[5]);
		}
			break;

		case "skylineRelevanceRecommender": {
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUTRESULTFILE);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			Double threshold = Double.parseDouble(cl.getOptionValue(OPT_RELEVANCE_THRESHOLD));
			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, DEFAULT_COMPLETE_INDEXES));
			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATIONSTRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);
			final int numberItemsRecommend = Integer
					.parseInt(cl.getOptionValue(OPT_ITEMSRECOMMENDED, DEFAULT_ITEMS_RECOMMENDED));

			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);
			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = RecommendationUtils
					.retrieveTrainTestIndexes(completeOrNot, trainFile, testFile, lp, lp);
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;
			List<Long> usersTest = indexes.v3;
			List<Long> itemsTest = indexes.v4;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
			FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

			FastPreferenceData<Long, Long> ranksysTestDataOriginal = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp), userIndexTest, itemIndexTest);

			FastPreferenceData<Long, Long> ranksysTrainDataOriginal = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp), userIndexTrain, itemIndexTrain);

			SkylineRelevanceRecommender<Long, Long> rec = new SkylineRelevanceRecommender<Long, Long>(
					ranksysTestDataOriginal, threshold);

			RecommendationUtils.ranksysWriteRanking(ranksysTrainDataOriginal, ranksysTestDataOriginal, rec, outputFile,
					numberItemsRecommend, RecommendationUtils.obtRecommendationStrategy(recommendationStrategy));
		}
			break;

		case "skylineAntiRelevanceRecommender": {
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUTRESULTFILE);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			Double antiRelTh = Double.parseDouble(cl.getOptionValue(OPT_ANTIRELEVANCETHRESHOLD));
			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, DEFAULT_COMPLETE_INDEXES));
			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATIONSTRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);
			final int numberItemsRecommend = Integer
					.parseInt(cl.getOptionValue(OPT_ITEMSRECOMMENDED, DEFAULT_ITEMS_RECOMMENDED));

			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);
			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = RecommendationUtils
					.retrieveTrainTestIndexes(completeOrNot, trainFile, testFile, lp, lp);
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;
			List<Long> usersTest = indexes.v3;
			List<Long> itemsTest = indexes.v4;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
			FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

			FastPreferenceData<Long, Long> ranksysTestDataOriginal = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp), userIndexTest, itemIndexTest);

			FastPreferenceData<Long, Long> ranksysTrainDataOriginal = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp), userIndexTrain, itemIndexTrain);

			SkylineAntiRelevanceRecommender<Long, Long> rec = new SkylineAntiRelevanceRecommender<Long, Long>(ranksysTestDataOriginal,
					antiRelTh);

			RecommendationUtils.ranksysWriteRanking(ranksysTrainDataOriginal, ranksysTestDataOriginal, rec, outputFile,
					numberItemsRecommend, RecommendationUtils.obtRecommendationStrategy(recommendationStrategy));
		}
			break;

		case "ranksysNonAccuracyMetricsEvaluation": {
			System.out.println(Arrays.toString(args));

			String outputFile = cl.getOptionValue(OPT_OUTRESULTFILE);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			String recommendedFile = cl.getOptionValue(OPT_RECOMMENDEDFILE);
			int threshold = Integer.parseInt(cl.getOptionValue(OPT_RELEVANCE_THRESHOLD));
			int antiRelevanceThreshold = Integer.parseInt(cl.getOptionValue(OPT_ANTIRELEVANCETHRESHOLD, "0"));
			String cutoffs = cl.getOptionValue(OPT_CUTOFF);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			String ranksysRelevanceModel = cl.getOptionValue(OPT_RANKSYSRELEVANCEMODEL);
			String ranksysDiscountModel = cl.getOptionValue(OPT_RANKSYSDISCOUNTMODEL);
			String ranksysBackground = cl.getOptionValue(OPT_RANKSYSBACKGROUND);
			String ranksysBase = cl.getOptionValue(OPT_RANKSYSBASE);

			File f = new File(outputFile);
			// If file of ranksys evaluation already exist then nothing
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			// we can filter out from test:

			final PreferenceData<Long, Long> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp));
			final PreferenceData<Long, Long> testData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp));

			final PreferenceData<Long, Long> totalData = new ConcatPreferenceData<>(trainData, testData);

			final Set<Long> testUsers = testData.getUsersWithPreferences().collect(Collectors.toSet());

			final PreferenceData<Long, Long> originalRecommendedData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(recommendedFile, lp, lp));

			// recommended data has to be filtered to avoid evaluating users not in test
			final PreferenceData<Long, Long> recommendedData = filterPreferenceData(originalRecommendedData, testUsers,
					null);

			// Binary relevance and anti relevance model for ranking metrics
			BinaryRelevanceModel<Long, Long> binRel = new BinaryRelevanceModel<>(false, testData, threshold);

			// Anti relevance model
			BinaryAntiRelevanceModel<Long, Long> selectedAntiRelevance = new BinaryAntiRelevanceModel<>(false, testData,
					antiRelevanceThreshold);
			// Relevance model for novelty/diversity (can be with or without relevance)
			RelevanceModel<Long, Long> selectedRelevance = null;

			double ranksysBackgroundD = 0.0;
			double ranksysBaseD = 0.0;

			if (ranksysBackground != null) {
				ranksysBackgroundD = Double.parseDouble(ranksysBackground);
			}

			if (ranksysBase != null) {
				ranksysBaseD = Double.parseDouble(ranksysBase);
			}

			if (ranksysRelevanceModel == null) {
				selectedRelevance = new NoRelevanceModel<>();
			} else {
				selectedRelevance = obtRelevanceModelRanksys(ranksysRelevanceModel, testData, threshold,
						ranksysBackgroundD);
			}

			RankingDiscountModel discModel = null;
			if (ranksysDiscountModel == null) {
				discModel = new NoDiscountModel();
			} else {
				discModel = obtRankingDiscountModel(ranksysDiscountModel, ranksysBaseD);
			}

			int numUsersTest = testData.numUsersWithPreferences();
			int numUsersRecommended = recommendedData.numUsersWithPreferences();
			int numItems = totalData.numItemsWithPreferences(); // Num items with preferences in the data
			System.out.println("\n\nNum users in the test set " + numUsersTest);
			System.out.println("\n\nNum users to whom we have made recommendations " + numUsersRecommended);

			Map<String, SystemMetric<Long, Long>> sysMetrics = new HashMap<>();

			// Some metrics need to be divided by the the users that have at least one
			// relevant item in the test set
			Map<String, RecommendationMetric<Long, Long>> recMetricsAvgRelUsers = new HashMap<>();
			// Some metrics need to be divided by the the users that have at least one
			// anti-relevant item in the test set
			Map<String, RecommendationMetric<Long, Long>> recMetricsAvgAntiRelUsers = new HashMap<>();
			// Some metrics need to be divided by the the users to whom we have made
			// recommendations
			Map<String, RecommendationMetric<Long, Long>> recMetricsAllRecUsers = new HashMap<>();

			String[] differentCutoffs = cutoffs.split(",");

			for (String cutoffS : differentCutoffs) {
				int cutoff = Integer.parseInt(cutoffS);
				addMetrics(recMetricsAvgRelUsers, recMetricsAvgAntiRelUsers, recMetricsAllRecUsers, threshold,
						antiRelevanceThreshold, cutoff, trainData, testData, selectedRelevance, binRel,
						selectedAntiRelevance, discModel);

				// SYSTEM METRICS. Only for normal evaluation, not per user
				sysMetrics.put("aggrdiv@" + cutoff, new AggregateDiversityMetric<>(cutoff, selectedRelevance));
				sysMetrics.put("gini@" + cutoff, new GiniIndex<>(cutoff, numItems));
			}

			// Average of all only for normal evaluation, not per user
			recMetricsAvgRelUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
					new AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<>(metric, binRel)));
			recMetricsAvgAntiRelUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
					new AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<>(metric, selectedAntiRelevance)));
			recMetricsAllRecUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
					new AverageRecommendationMetric<>(metric, numUsersRecommended)));

			RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);

			format.getReader(recommendedFile).readAll()
					.forEach(rec -> sysMetrics.values().forEach(metric -> metric.add(rec)));

			PrintStream out = new PrintStream(new File(outputFile));
			sysMetrics.forEach((name, metric) -> out.println(name + "\t" + metric.evaluate()));
			out.close();

		}
			break;

		}

	}

	/***
	 * Method to add the metrics in their corresponding user maps
	 * 
	 * @param recMetricsAvgRelUsers
	 *            metrics that will be divided by the number of users that have at
	 *            least one relevant item in the test set
	 * @param recMetricsAvgAntiRelUsers
	 *            metrics that will be divided by the number of users that have at
	 *            least one anti-relevant item in the test set
	 * @param recMetricsAllRecUsers
	 *            metrics that will be divided by the number of users to whom we are
	 *            recommending
	 * @param threshold
	 *            variable to consider that a item is relevant if the rating in the
	 *            test set is higher or equal this threshold
	 * @param thresholdAntiRelevance
	 *            variable to consider that a item is anti-relevant if the rating in
	 *            the test set is lower or equal this threshold
	 * @param cutoff
	 *            the number of top-N recommendations to consider
	 * @param trainData
	 *            the trainData
	 * @param testData
	 *            the testData
	 * @param selectedRelevance
	 *            the relevance model (novelty)
	 * @param binRel
	 *            the binary relevance model for relevance metrics
	 * @param antiBinRel
	 *            the anti binary relevance model for anti-relevance metrics
	 * @param discModel
	 *            the discount model
	 * @param antimetricsFlags
	 *            flag in order to compute the anti-metrics
	 * @param computeOnlyAcc
	 *            flag not to compute novelty and diversity metrics
	 * @throws IOException
	 */
	public static void addMetrics(Map<String, RecommendationMetric<Long, Long>> recMetricsAvgRelUsers,
			Map<String, RecommendationMetric<Long, Long>> recMetricsAvgAntiRelUsers,
			Map<String, RecommendationMetric<Long, Long>> recMetricsAllRecUsers, int threshold,
			int thresholdAntiRelevance, int cutoff, PreferenceData<Long, Long> trainData,
			PreferenceData<Long, Long> testData, RelevanceModel<Long, Long> selectedRelevance,
			BinaryRelevanceModel<Long, Long> binRel, BinaryAntiRelevanceModel<Long, Long> antiBinRel,
			RankingDiscountModel discModel) throws IOException {

		recMetricsAvgRelUsers.put("Precision@" + cutoff + "_" + threshold, new Precision<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("MAP@" + cutoff + "_" + threshold, new AveragePrecision<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("Recall@" + cutoff + "_" + threshold,
				new es.uam.eps.ir.ranksys.metrics.basic.Recall<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("MRR@" + cutoff + "_" + threshold, new ReciprocalRank<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("NDCG@" + cutoff + "_" + threshold,
				new NDCG<>(cutoff, new NDCG.NDCGRelevanceModel<>(false, testData, threshold)));
		recMetricsAvgRelUsers.put("Success@" + cutoff + "_" + threshold, new Success<>(cutoff, binRel));

		// All ratios divided by rec
		recMetricsAllRecUsers.put("BorderlineRatio@" + cutoff + "_" + threshold + "_" + thresholdAntiRelevance,
				new BorderlineRatio<>(cutoff, testData, threshold, thresholdAntiRelevance));
		recMetricsAllRecUsers.put("UnknownItemsRatio@" + cutoff + "_" + threshold + "_" + thresholdAntiRelevance,
				new UnknownItemsRatio<>(cutoff, testData));
		recMetricsAllRecUsers.put("AntiRelevanceRatio@" + cutoff + "_" + thresholdAntiRelevance,
				new AntiRelevanceRatio<>(cutoff, antiBinRel));
		recMetricsAllRecUsers.put("RelevanceRatio@" + cutoff + "_" + threshold, new Precision<>(cutoff, binRel));

		recMetricsAllRecUsers.put("epc@" + cutoff,
				new EPC<>(cutoff, new PCItemNovelty<>(trainData), selectedRelevance, discModel));
		recMetricsAllRecUsers.put("efd@" + cutoff,
				new EFD<>(cutoff, new FDItemNovelty<>(trainData), selectedRelevance, discModel));

		recMetricsAvgAntiRelUsers.put("AntiSuccess@" + cutoff + "_" + thresholdAntiRelevance,
				new AntiRecommendationMetricComplement<>(new AntiSuccess<>(cutoff, antiBinRel)));
		recMetricsAvgAntiRelUsers.put("AntiPrecision@" + cutoff + "_" + thresholdAntiRelevance,
				new AntiRecommendationMetricComplement<>(new Precision<>(cutoff, antiBinRel)));
		recMetricsAvgAntiRelUsers.put("AntiMAP@" + cutoff + "_" + thresholdAntiRelevance,
				new AntiRecommendationMetricComplement<>(new AveragePrecision<>(cutoff, antiBinRel)));
		recMetricsAvgAntiRelUsers.put("AntiRecall@" + cutoff + "_" + thresholdAntiRelevance,
				new AntiRecommendationMetricComplement<>(
						new es.uam.eps.ir.ranksys.metrics.basic.Recall<>(cutoff, antiBinRel)));
		recMetricsAvgAntiRelUsers.put("AntiMRR@" + cutoff + "_" + thresholdAntiRelevance,
				new AntiRecommendationMetricComplement<>(new ReciprocalRank<>(cutoff, antiBinRel)));
		recMetricsAvgAntiRelUsers.put("AntiNDCG@" + cutoff + "_" + thresholdAntiRelevance,
				new AntiRecommendationMetricComplement<>(new AntiNDCG<>(cutoff,
						new AntiNDCG.NDCGAntiRelevanceModel<>(false, testData, thresholdAntiRelevance))));

	}

	/***
	 * Method to retrieve the train test indexes from a file
	 * 
	 * @param completeIndexes
	 *            flag indicating if we are going to concatenate the indexes of
	 *            train and test or not
	 * @param trainFile
	 *            the train file
	 * @param testFile
	 *            the test file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple4<List<U>, List<I>, List<U>, List<I>> retrieveTrainTestIndexes(boolean completeIndexes,
			String trainFile, String testFile, Parser<U> up, Parser<I> ip) {
		List<U> usersTrain = null;
		List<I> itemsTrain = null;
		List<U> usersTest = null;
		List<I> itemsTest = null;
		if (completeIndexes) {
			Tuple2<List<U>, List<I>> userItems = getCompleteUserItems(trainFile, testFile, up, ip);
			usersTrain = userItems.v1;
			itemsTrain = userItems.v2;
			usersTest = userItems.v1;
			itemsTest = userItems.v2;
		} else {
			Tuple2<List<U>, List<I>> userItemsTrain = getUserItemsFromFile(trainFile, up, ip);
			usersTrain = userItemsTrain.v1;
			itemsTrain = userItemsTrain.v2;
			Tuple2<List<U>, List<I>> userItemsTest = getUserItemsFromFile(testFile, up, ip);
			usersTest = userItemsTest.v1;
			itemsTest = userItemsTest.v2;

		}
		return new Tuple4<>(usersTrain, itemsTrain, usersTest, itemsTest);
	}

	/***
	 * Method to obtain the user and the items from the users and items files
	 * 
	 * @param fileTrain
	 *            the train file
	 * @param fileTest
	 *            the test file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple2<List<U>, List<I>> getCompleteUserItems(String fileTrain, String fileTest, Parser<U> up,
			Parser<I> ip) {
		try {
			PreferenceData<U, I> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTrain, up, ip));
			PreferenceData<U, I> testData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTest, up, ip));
			PreferenceData<U, I> totalData = new ConcatPreferenceData<>(trainData, testData);
			List<U> usersList = totalData.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = totalData.getAllItems().collect(Collectors.toList());

			System.out.println("Ordering by longs");

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	/***
	 * Method to obtain a tuple of users and items from a file
	 * 
	 * @param file
	 *            the file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple2<List<U>, List<I>> getUserItemsFromFile(String file, Parser<U> up, Parser<I> ip) {
		try {
			PreferenceData<U, I> data = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(file, up, ip));
			List<U> usersList = data.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = data.getAllItems().collect(Collectors.toList());

			System.out.println("Ordering by longs");

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			System.out.println("Exception catched");
			e.printStackTrace();
			return null;
		}

	}

	/***
	 * Method to filter the preference data by providing the set of valid users and
	 * valid items
	 * 
	 * @param original
	 *            the original preferences
	 * @param validUsers
	 *            the set of valid users
	 * @param validItems
	 *            the set of valid items
	 * @return the preference data filtered
	 */
	public static <U, I> PreferenceData<U, I> filterPreferenceData(PreferenceData<U, I> original, Set<U> validUsers,
			Set<I> validItems) {
		final List<Tuple3<U, I, Double>> tuples = new ArrayList<>();
		original.getUsersWithPreferences().filter(u -> validUsers.contains(u)).forEach(u -> {
			if (validItems != null) {
				original.getUserPreferences(u).filter(t -> (validItems.contains(t.v1))).forEach(idPref -> {
					tuples.add(new Tuple3<>(u, idPref.v1, idPref.v2));
				});
			} else {
				original.getUserPreferences(u).forEach(idPref -> {
					tuples.add(new Tuple3<>(u, idPref.v1, idPref.v2));
				});
			}
		});
		System.out.println("Tuples original: " + original.numPreferences());
		System.out.println("Tuples original filtered: " + tuples.size());
		Stream<Tuple3<U, I, Double>> prev = tuples.stream();
		PreferenceData<U, I> result = SimplePreferenceData.load(prev);
		return result;
	}

	/**
	 * Method to obtain the ranking discount model
	 * 
	 * @param model
	 *            the model in a string
	 * @param base
	 *            only required for the exponential discount model
	 * @return the RankSys Discount model
	 */
	public static <U, I> RankingDiscountModel obtRankingDiscountModel(String model, double base) {
		switch (model) {
		case "ExponentialDiscountModel":
			return new ExponentialDiscountModel(base);
		case "LogarithmicDiscountModel":
			return new LogarithmicDiscountModel();
		case "NoDiscountModel":
			return new NoDiscountModel();
		case "ReciprocalDiscountModel":
			return new ReciprocalDiscountModel();
		default:
			return null;
		}
	}

	/**
	 * Method to obtain the relevance model from RankSys
	 * 
	 * @param model
	 *            String of the relevance model
	 * @param prefData
	 *            the preference data
	 * @param threshold
	 *            the threshold
	 * @param background
	 *            only required for background
	 * @return the relevance model
	 */
	public static <U, I> RelevanceModel<U, I> obtRelevanceModelRanksys(String model, PreferenceData<U, I> prefData,
			int threshold, double background) {
		switch (model) {
		case "BinaryRelevanceModel":
			return new BinaryRelevanceModel<>(false, prefData, threshold);
		case "NoRelevanceModel":
			return new NoRelevanceModel<U, I>();
		case "BackgroundBinaryRelevanceModel":
			return new BackgroundBinaryRelevanceModel<>(false, prefData, threshold, background);
		default:
			return null;
		}
	}

	/**
	 * 
	 * 
	 * 
	 * /** Method that will obtain a command line using the available options of the
	 * arguments
	 *
	 * @param args
	 *            the arguments that the program will receive
	 * @return a command line if the arguments are correct or null if an error
	 *         occurred
	 */
	private static CommandLine getCommandLine(String[] args) {
		Options options = new Options();

		// Number of the case
		Option caseIdentifier = new Option("o", OPT_CASE, true, "option of the case");
		caseIdentifier.setRequired(true);
		options.addOption(caseIdentifier);

		// Train file
		Option trainFile = new Option("trf", OPT_TRAINFILE, true, "input file train path");
		trainFile.setRequired(true);
		options.addOption(trainFile);

		// Here not required
		// TestFile file
		Option testFile = new Option("tsf", OPT_TESTFILE, true, "input file test path");
		testFile.setRequired(false);
		options.addOption(testFile);

		// Neighbours
		Option neighbours = new Option("n", OPT_NEIGH, true, "neighbours");
		neighbours.setRequired(false);
		options.addOption(neighbours);

		// NumberItemsRecommended
		Option numberItemsRecommended = new Option("nI", OPT_ITEMSRECOMMENDED, true, "Number of items recommended");
		numberItemsRecommended.setRequired(false);
		options.addOption(numberItemsRecommended);

		// OutResultfile
		Option outfile = new Option("orf", OPT_OUTRESULTFILE, true, "output result file");
		outfile.setRequired(false);
		options.addOption(outfile);

		// Ranksys similarity
		Option rankSysSim = new Option("rs", OPT_RANKSYS_SIM, true, "ranksys similarity");
		rankSysSim.setRequired(false);
		options.addOption(rankSysSim);

		// Overwrite result
		Option outputOverwrite = new Option("ovw", OPT_OVERWRITE, true, "overwrite");
		outputOverwrite.setRequired(false);
		options.addOption(outputOverwrite);

		// Overwrite result

		// Feature reading file
		Option recommendedFile = new Option("rf", OPT_RECOMMENDEDFILE, true, "recommended file");
		recommendedFile.setRequired(false);
		options.addOption(recommendedFile);

		// RankSysMetric
		Option ranksysCutoff = new Option("rc", OPT_CUTOFF, true, "ranksyscutoff");
		ranksysCutoff.setRequired(false);
		options.addOption(ranksysCutoff);

		// RankSys factorizers (k)
		Option kFactorizer = new Option("kFactorizer", OPT_KFACTORIZER, true, "k factorizer");
		kFactorizer.setRequired(false);
		options.addOption(kFactorizer);

		// RankSys factorizers (alpha)
		Option alhpaFactorizer = new Option("aFactorizer", OPT_ALPHAFACTORIZER, true, "alpha factorizer");
		alhpaFactorizer.setRequired(false);
		options.addOption(alhpaFactorizer);

		// RankSys factorizers (lambda)
		Option lambdaFactorizer = new Option("lFactorizer", OPT_LAMBDAFACTORIZER, true, "lambda factorizer");
		lambdaFactorizer.setRequired(false);
		options.addOption(lambdaFactorizer);

		// RankSys factorizers (numInteractions)
		Option numInteractionsFact = new Option("nIFactorizer", OPT_NUMINTERACTIONSFACTORIZER, true,
				"numInteractions factorizer");
		numInteractionsFact.setRequired(false);
		options.addOption(numInteractionsFact);

		// RansksyLibrary NonAccuracyEvaluationParameters
		Option ranksysRelevanceModel = new Option("ranksysRelModel", OPT_RANKSYSRELEVANCEMODEL, true,
				"ranksys relevance model");
		ranksysRelevanceModel.setRequired(false);
		options.addOption(ranksysRelevanceModel);

		Option ranksysDiscountModel = new Option("ranksysDiscModel", OPT_RANKSYSDISCOUNTMODEL, true,
				"ranksys discount model");
		ranksysDiscountModel.setRequired(false);
		options.addOption(ranksysDiscountModel);

		Option ranksysBackground = new Option("ranksysBackRel", OPT_RANKSYSBACKGROUND, true,
				"ranksys background for relevance model");
		ranksysBackground.setRequired(false);
		options.addOption(ranksysBackground);

		Option thresholdSim = new Option("thr", OPT_RELEVANCE_THRESHOLD, true, "Matching threshold");
		thresholdSim.setRequired(false);
		options.addOption(thresholdSim);

		Option ranksysBase = new Option("ranksysBaseDisc", OPT_RANKSYSBASE, true, "ranksys base for discount model");
		ranksysBase.setRequired(false);
		options.addOption(ranksysBase);

		Option computeAlsoTest = new Option("recAndTest", OPT_COMPUTERECANDTEST, true,
				"if we divide by the number of users recommended or also the test");
		computeAlsoTest.setRequired(false);
		options.addOption(computeAlsoTest);

		Option antiRelevanceThreshold = new Option("antiRelTh", OPT_ANTIRELEVANCETHRESHOLD, true,
				"integer in order to indicate the antiRelevanceTh");
		antiRelevanceThreshold.setRequired(false);
		options.addOption(antiRelevanceThreshold);

		Option useCompleteIndex = new Option("cIndex", OPT_COMPLETEINDEXES, true,
				"use the complete indexes (train + test)");
		useCompleteIndex.setRequired(false);
		options.addOption(useCompleteIndex);

		Option rankSysRecommender = new Option("rr", OPT_RANKSYS_REC, true, "ranksys recommeder");
		rankSysRecommender.setRequired(false);
		options.addOption(rankSysRecommender);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			return null;
		}
		return cmd;

	}
}
