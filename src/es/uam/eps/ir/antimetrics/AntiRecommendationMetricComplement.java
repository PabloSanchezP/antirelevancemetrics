/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;

/**
 * Class to take the complement of any Anti-Recommendation Metric Component
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> type of the users
 * @param <I> type of the items
 */
public class AntiRecommendationMetricComplement<U, I> extends AbstractRecommendationMetric<U, I> {
	private final RecommendationMetric<U, I> antiRecommendationMetric;

	public AntiRecommendationMetricComplement(RecommendationMetric<U, I> antiRecommendationMetric) {
		this.antiRecommendationMetric = antiRecommendationMetric;
	}

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		return 1 - this.antiRecommendationMetric.evaluate(recommendation);
	}

}
