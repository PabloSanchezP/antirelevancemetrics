/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics.ratio;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.antimetrics.antirel.BinaryAntiRelevanceModel;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel.UserRelevanceModel;

/***
 * Method that will retrieve the ratio of items that are anti-relevant ones
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> type of the users
 * @param <I> type of the items
 */
public class AntiRelevanceRatio<U, I> extends AbstractRecommendationMetric<U, I> {

	private final int cutOff;

	private final BinaryAntiRelevanceModel<U, I> antiRelModel;

	public AntiRelevanceRatio(int cutoff, BinaryAntiRelevanceModel<U, I> antiRelModel) {
		this.cutOff = cutoff;
		this.antiRelModel = antiRelModel;
	}

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		UserRelevanceModel<U, I> userRelModel = antiRelModel.getModel(recommendation.getUser());

		// Although it is called "isRelevant", it actually refers to the anti-relevant items
		return recommendation.getItems().stream().limit(cutOff).map(Tuple2od::v1).filter(userRelModel::isRelevant) 
				.count() / (double) cutOff;
	}

}
