/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.antimetrics.ratio;

import java.util.Set;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.preference.IdPref;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;

/**
 * Metric that will return the percentage of items that are rated between the
 * relevance and anti-relevance thresholds
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> type of the users
 * @param <I> type of the items
 */
public class BorderlineRatio<U, I> extends AbstractRecommendationMetric<U, I> {

	private final int cutOff;
	private final int relTh;
	private final int antiRel;

	private final PreferenceData<U, I> dataTest;

	public BorderlineRatio(int cutoff, PreferenceData<U, I> testData, int relTh, int antiRel) {
		this.cutOff = cutoff;
		this.dataTest = testData;
		this.antiRel = antiRel;
		this.relTh = relTh;
	}

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		Set<I> itemsRatedBorderline = dataTest.getUserPreferences(recommendation.getUser())
				.filter(pref -> pref.v2 < this.relTh && pref.v2 > this.antiRel).map(IdPref::v1)
				.collect(Collectors.toSet());

		return recommendation.getItems().stream().limit(cutOff).map(Tuple2od::v1)
				.filter(item -> itemsRatedBorderline.contains(item)).count() / (double) cutOff;
	}

}
