# AntiRelevanceMetrics

# README #

Framework of the anti-relevance metrics defined in the paper:

[1] Pablo Sánchez, Alejandro Bellogín. Measuring anti-relevance: a study on when recommendation algorithms produce bad suggestions. In RecSys 2018.

### Prerequisites

Maven

Java 8

## Dependencies

[RankSys 0.4.3](https://github.com/RankSys/RankSys/releases/tag/0.4.3)

### Instructions ###

* Steps to install the library and download all the data and scripts:
    * git clone https://bitbucket.org/PabloSanchezP/AntiRelevanceMetrics
    * cd antirelevancemetrics
    * mvn install
    * The generated JAR is in target\AntiRelevanceMetrics-0.0.1-SNAPSHOT.jar

* If you want to use the library inside your (Java) project:
    * Add the JitPack dependency as described in [https://jitpack.io/#org.bitbucket.PabloSanchezP/timeawarenoveltymetrics](https://jitpack.io/#org.bitbucket.PabloSanchezP/timeawarenoveltymetrics)

* Running tests: execute the scripts in the following order:
    * step0_prepareJAR.sh
    * step1_generateSplits.sh
    * step2_recommendation_Evaluation.sh
    * step3_postEval.sh


## Additional algorithms/frameworks used in the experiments
  * [MyMedialite](http://www.mymedialite.net/) (for BPR)

## Authors

  * **Pablo Sánchez** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)
  * **Alejandro Bellogín** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)

## Contact

  * **Pablo Sánchez** - <pablo.sanchezp@uam.es>

## License

  This project is licensed under [the GNU GPLv3 License](LICENSE.txt)

## Acknowledgments

  * This work has been funded by the Ministerio de Economía y Competitividad within the 2017 call for predoctoral contracts co-funded by the European Social Fund (ESF) and the project TIN2016-80630-P.
