: '
  First script to be executed.
  -Firstly, it processes the Movielens dataset and split the fields by tabulation (user, item, rating and timestamp)
  -Secondly, it processes the LastFm dataset and transform the implicit weights into explicit ratings
  -After that, we perform the splits using the RiVal framework (integrated in our framework) in order to create a 5-fold validation for both datasets
  -Finally, we download MyMediaLite framework
'
# Variable to configure
javaCommand=java
JAR=target/AntiMetrics.jar
jvmMemory=-Xmx12G



dirOriginalDatasets=OriginalDatasets
dirProcesedDatasets=TransformedData

mkdir -p $dirProcesedDatasets

# OriginalDatasets
fileOriginalMovielens1M=Original_Movielens1M.dat
fileOriginalLastfmHetRec=Original_LastfmHetrec.dat

ttpathfiles=TrainTest


# Movielens if normed by longs, but we need to use tabulation
if [ ! -f $dirProcesedDatasets/Movielens1M_NoRep_Parsed.txt ]; then
  sed 's/::/\t/g' $dirOriginalDatasets/$fileOriginalMovielens1M > $dirProcesedDatasets/Movielens1M_NoRep_Parsed.txt
fi

# Lastfm does not need to be parsed, but need to be transformed to explicit
if [ ! -f $dirProcesedDatasets/Lastfm_NoRep_Parsed_Explicit.txt ]; then
  $javaCommand $jvmMemory -jar $JAR -o implicitToExplicit -trf $dirOriginalDatasets/$fileOriginalLastfmHetRec $dirProcesedDatasets/Lastfm_NoRep_Parsed_Explicit.txt "\t"
fi


# Now, begin splits
if [ ! -d "$ttpathfiles" ]; then
  mkdir -p $ttpathfiles
  # For Movielens1M
  java $jvmMemory -jar $JAR -o splitsRival -trf $dirProcesedDatasets/Movielens1M_NoRep_Parsed.txt CrossValidation 5 false ./$ttpathfiles/ true Movielens1M_NoRep_Parsed _global.train mMovielens1M_NoRep_Parsed _global.test

  # For Lastfm
  java $jvmMemory -jar $JAR -o splitsRival -trf $dirProcesedDatasets/Lastfm_NoRep_Parsed_Explicit.txt CrossValidation 5 false ./$ttpathfiles/ true Lastfm_NoRep_Parsed_Explicit _global.train mLastfm_NoRep_Parsed_Explicit _global.test
fi

# Download mymedialite and unzip it
if [ ! -d "MyMediaLite-3.11" ]; then
  wget "http://mymedialite.net/download/MyMediaLite-3.11.tar.gz"
  tar zxvf MyMediaLite-3.11.tar.gz
fi
