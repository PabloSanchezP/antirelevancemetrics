: '
	Script that allows us to obtain a summary file containing the results of all datasets (one file per dataset)
	containing the results of all recommenders generated for that dataset
'

#!/bin/sh



resultsFolder=ResultsFolder
mkdir -p $resultsFolder
resultsPrefix=eval
nonaccresultsPrefix=naeval




for dataset in Movielens1M_NoRep_Parsed Lastfm_NoRep_Parsed_Explicit
do
	summary_file=$dataset"_summary".txt
	rm $summary_file
	for fold in 0 1 2 3 4
	do
		for evthreshold in 4
		do
			find $resultsFolder/ -name "$nonaccresultsPrefix"_""RelEvTh""$evthreshold*"$dataset""$fold"* | while read recFile; do
				recFileName=$(basename "$recFile" .txt) # extension removed
				awk -v FILE=$summary_file -v TH=$evthreshold -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"TH"\t"$0 >> FILE }' $recFile
			done # End resultFiles
			wait
		done # End evth
		wait
	done # End fold
	wait
done # End dataset
wait
